# CACI-sandbox vol 1.

This site and linked files are to serve the purpose of creating a kubernetes sandbox for developers. It also serves a secondary purpose of allowing developers to hone and sharpen their DevSecOp Skills. Each day new modules and projects could be added to help developers create solutions to issues they may face. Let's get started. 

## Install Helm
Steps are provided below and additional docs can be found on the [Helm](https://helm.sh/docs/intro/install/) site.

In your terminal type:
> $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 <br>
> $ chmod 700 get_helm.sh <br>
> $ ./get_helm.sh <br>

Test the install by typing:
>> helm version <br>

![successful helm install](images/2_successful_helm_install.png)

The output should look similar to the following <br><br>
***version.BuildInfo{Version:"v3.11.0", GitCommit:"472c5736ab01133de504a826bd9ee12cbe4e7904", GitTreeState:"clean", GoVersion:"go1.18.10"}*** <br>

There is a slightly more difficult way to install helm using GO and a working GOLANG environment ***NOTE: GO IS A PREREQUISITE FOR THIS WAY OF INSTALLATION*** <br><br>
> $ git clone https://github.com/helm/helm.git <br>
> $ cd helm <br>
> $ make <br>

Test the install by typing:
>> helm version <br>

## Install Kubernetes
Next, after Helm has been installed, we can install Kubernetes (Kube). The order in which kube and helm are installed does not seem to matter. You'll want to follow the install instructions for [Kube on Linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/). It's important to note that once you follow the instructions under 'Install kubectl binary with curl on Linux' That you go to the section underneath the Red Hat Based Distributions and type that information into your terminal as well. 

![successful kube install](images/3_successful_kube_install.png)

### Launch your Cluster 

Coming Soon. . . 

## GitLab Runner
Once you have your cluster up and running, configure a Kubernetes GitLab Agent to connect your cluster to Git. From the Project menu in GitLab, go to infrastructure on the left hand side of the page. Once you click infrastructure, click kubernetes cluster, then click connect to a cluster. Type in an agent name, then click create. You'll see a modal box with information similar to the following:

> ### Agent Access Token
> dqMMViUkZNAn_UHBTHKHsgsDm9fsjNUHVuTqb1qvMZxFkWQ9nQ <br>

> ### Install Using helm
> helm repo add gitlab https://charts.gitlab.io <br>
> helm repo update <br>
> helm upgrade --install kube-agent gitlab/gitlab-agent \ <br>
>    --namespace gitlab-agent-kube-agent \ <br>
>    --create-namespace \ <br>
>    --set image.tag=v15.10.0 \ <br>
>    --set config.token=dqMMViUkZNAn_UHBTHKHsgsDm9fsjNUHVuTqb1qvMZxFkWQ9nQ \ <br>
>    --set config.kasAddress=wss://kas.gitlab.com <br>

A successful agent-cluster connection should look similar to the following: <br>
![Successful Agent Install](images/1_succesful_agent_install.png) <br>
![Successful Agent Connection](images/4_successful_agent_connection.png)<br><br>

#### Errors to work thru
A potential error may stem from not having the proper admin priveleges or not having correct certificates, it would look similar to: <br><br>

>> Error: looks like "https://charts.gitlab.io" is not a valid chart repository or cannot be reached: Get "https://charts.gitlab.io/index.yaml": x509: certificate is valid for udev-caci.com, 65 216.151.125, 63.118.47.119, 65.201.168.195, 65.201.131.67, 65.216.151.125, 63.118.47.119, 65.201.168.195, 65.201.131.67, not charts.gitlab.io


## DevOps the Hard way (not really)

We start by pulling code from here: [DevOps Quickstart Kubes](https://github.com/AdminTurnedDevOps/Kubernetes-Quickstart-Environments.git)


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jonesa-caci/K8s-Sandbox.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/jonesa-caci/K8s-Sandbox/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
